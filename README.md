# ipm - IPFS Pin Management

## About ipm
ipm was partially inspired by [vpm](https://github.com/netzverweigerer/vpm) to act as a helper to the [ipfs-go](https://github.com/ipfs/go-ipfs/) command-line tool for [IPFS](https://docs.ipfs.io/concepts/what-is-ipfs/), with a specific focus on pinned content.

As of now, `ipfs-go` can give a list of pinned items with `ipfs ls`. Depending on the amount of pinned items, the output can be overwhelming. Even showing just recursive items offers the file hashes with no detail on what exactly the item is. Keeping track of pins becomes a hassle of matching up hashes with descriptions/names.

With `ipm`, additional management is done automatically. Any additions or removals are all taken into account and labeled with either the original file name or one of your choosing, and indexed within a database. Pins are also automatically symlinked to your selected `$IPM_HOME/mounts` directory through the use of `ipfs mount` (which must be set up prior to use). IPFS hashes don't have to be remembered or kept track of manually.

## Installation

1. Make sure `ipfs-go` is already initialized on your system and the mount points set-up with `ipfs mount`

2. Clone this repository

```
$ git clone https://codeberg.org/savoy/ipm
```

3. Set-up your $IPM_HOME

```
$ echo 'export IPM_HOME="</path/to/dir/>"' >> ~/.profile
```

No external dependencies are required. Optional dependencies include [nnn](https://github.com/jarun/nnn) for built-in file manager view, [exa](https://github.com/ogham/exa) for additional info instead of the default of `ls`, and [argcomplete](https://github.com/kislyuk/argcomplete) for shell completion of pinned items by its given filename.

## Example Usage

Run `ipm <arg> -h` for additional information on each command.

Adding a file or a directory works seamlessly, as does pinning an already existing IPFS hash. The item will be pinned to your IPFS node, the listing will be entered into your pins database, and the mount point will be symlinked to your `$IPM_HOME/mounts` directory with the description/name given (if none given, defaults to the original filename or hash).

```
$ ipm add /path/to/file -d <optional description/name>
$ ipm add Qm... -d <optional description/name>
```

Listing your pins will be a simple `ls -lh` into your `$IPM_HOME/mounts`, with optional usage with the `nnn` filebrowser or `exa`. Can also `ls` a pinned directory, with shell autocompletion (if installed) available for all items.

```
$ ipm ls
$ ipm ls ipfs_startup_readme
```

Pin removal is all based on the symlink names (with shell autocompletion if enabled). The removal will unpin the item, remove the entry from the database, and delete the symlink from $IPM_HOME/mounts.

```
$ ipm rm ipfs_startup_readme
```
